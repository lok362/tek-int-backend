import * as express from "express";
import { resolve } from "path";
import * as bodyParser from "body-parser";
import { getApiRouter } from "./api";
import * as lowdb from "lowdb";
import * as FileAsync from "lowdb/adapters/FileAsync";

const PORT = 3000;

(async function launch() {
    const adapter = new FileAsync<DBI>(resolve(__dirname, "db.json"));
    const db = await lowdb(adapter);
    await db.defaults({ images: [] }).write();

    const server = express();
    server.use(bodyParser.json({ limit: "8mb" }));

    server.use("/api", await getApiRouter(db));

    server.use(express.static(resolve(__dirname, "..", "www")));

    server.listen(PORT, () => {
        console.log(`Server listening on ${PORT}`);
    });
})().catch(err => {
    console.log(err);
    process.exit(1);
});

export interface DBI {
    images: GalleryImage[];
}

export interface GalleryImage {
    id: string;
    name: string;
    data: string;
}
