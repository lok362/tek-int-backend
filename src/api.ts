import * as express from "express";
import { LowdbAsync } from "../node_modules/@types/lowdb";
import { DBI, GalleryImage } from "./";

export async function getApiRouter(db: LowdbAsync<DBI>) {
    const router = express.Router();

    router.get("/image", (req, res) => {
        const images = db.get("images").value();
        return res.json({ success: true, images });
    });

    router.get("/image/:id", (req, res) => {
        const id = req.params.id as string;
        const image = db.get("images").find({ id }).value();
        if (!image) {
            res.status(404);
            return res.json({ success: false, reason: "File not found!" });
        } else {
            return res.json({ success: true, image });
        }
    });

    router.delete("/image/:id", async (req, res) => {
        const id = req.params.id as string;
        const image = db.get("images").find({ id }).value();
        if (!image) {
            res.status(404);
            return res.json({ success: false, reason: "File not found!"});
        } else {
            const dbi = db.getState();
            const index = dbi.images.findIndex(img => img.id === id);
            if (index !== -1) dbi.images.splice(index, 1);
            db.setState(dbi);
            return res.json({ success: true });
        }
    });

    router.post("/image", async (req, res) => {
        const name = req.body.name;
        const data = req.body.data;

        let n = db.get("images").value().length;
        while (db.get("images").find({ id: n }).value() === n) n++;
        const id = n.toString();

        const image: GalleryImage = { id, name, data };
        await db.get("images").push(image).write();

        return res.json({ success: true, id });
    });

    return router;
}
